require "test_helper"

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    user = users(:one)
    sign_in user
  end

  test "get all articles should work" do
    get api_articles_path
    assert_response :success
  end

  test "get article 1 should work" do
    get "/api/articles/1"
    assert_response :success
  end

  test "raises RecordNotFound when not found" do
    assert_raises(ActiveRecord::RecordNotFound) do
      get "/api/articles/50"
    end
  end

  test "creates articles works" do
    post api_articles_path, params: { article: { title: "Ahoy!", body: "long body y'al", status: "public" } }
    #expected = Article.last
    #actual = Article.new(response.parsed_body)
    assert_equal(Article.last, Article.new(response.parsed_body))
    assert_response :success
  end

end

