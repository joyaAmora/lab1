require "test_helper"

class CommentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "get all comments for an article should work" do
    get "/api/articles/1/comments"
    assert_response :success
  end

  test "get 1 comment from 1 artcile should work" do
    get "/api/articles/1/comments/2"
    assert_response :success
  end

  test "raises RecordNotFound when not found" do
    assert_raises(ActiveRecord::RecordNotFound) do
      get "/api/articles/1/comments/50"
    end
  end

  test "creates comment works" do
    post "/api/articles/2/comments", params: { comment: { commenter: "Luke", body: "congrats", status: "public" } }
    assert_equal(Comment.last, Comment.new(response.parsed_body))
    assert_response :success
  end

  test "delete comment works" do
    user = users(:one)
    sign_in user
    delete "/api/articles/1/comments/1"
    assert_response :success
  end

  test "raises RecordNotFound when try to delete comment with invalid id" do
    user = users(:one)
    sign_in user
    assert_raises(ActiveRecord::RecordNotFound) do
      delete "/api/articles/2/comments/2"
    end
  end

  test "delete comment won't work if not logged in" do
    delete "/api/articles/1/comments/1"
    assert_response :unauthorized
  end
end
