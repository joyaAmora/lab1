require "test_helper"

class CommentTest < ActiveSupport::TestCase
  include Devise::Test::IntegrationHelpers
  # test "the truth" do
  #   assert true
  # end
  test "should save comment will pass" do
    article = Article.find(1)
    comment = article.comments.create(commenter: "john", body: "doe", status: "public")
    assert comment.save
  end

  test "should not save comment without commenter, body or status will pass" do
    article = Article.find(1)
    comment = article.comments.create()
    assert_not comment.save
    assert_equal(["Status is not included in the list", "Commenter can't be blank", "Body can't be blank"], comment.errors.full_messages)
  end

  test "should not save comment without commenter will pass" do
    article = Article.find(1)
    comment = article.comments.create(body: "doe", status: "public")
    assert_not comment.save
    assert_equal(["Commenter can't be blank"], comment.errors.full_messages)
  end

  test "should not save comment without body will pass" do
    article = Article.find(1)
    comment = article.comments.create(commenter: "paul", status: "public")
    assert_not comment.save
    assert_equal(["Body can't be blank"], comment.errors.full_messages)
  end

  test "should not save comment without status will pass" do
    article = Article.find(1)
    comment = article.comments.create(commenter: "bob", body: "patates au four")
    assert_not comment.save
    assert_equal(["Status is not included in the list"], comment.errors.full_messages)
  end

  test "should not save comment with wrong status will pass" do #right status are: ['public', 'private', 'archived']
    article = Article.find(1)
    comment = article.comments.create(commenter: "bob", body: "patates au four", status: "b")
    assert_not comment.save
    assert_equal(["Status is not included in the list"], comment.errors.full_messages)
  end
end
