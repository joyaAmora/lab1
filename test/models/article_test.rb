require "test_helper"

class ArticleTest < ActiveSupport::TestCase
  include Devise::Test::IntegrationHelpers
  # test "the truth" do
  #   assert true
  # end
  test "should save article will pass" do
    article = Article.new(title: "test qui passe", body: "ce test est valide", status: "private")
    assert article.save
  end

  test "should not save article without title, body or status will pass" do
    article = Article.new
    assert_not article.save
  end

  test "should not save article without title will pass" do
    article = Article.new(body: "0123456789" , status: "public")
    assert_not article.save
  end

  test "should not save article without body will pass" do
    article = Article.new(title: "patates", status: "public")
    assert_not article.save
  end

  test "should not save article with body.length < 10 will pass" do
    article = Article.new(title: "patates", body: "1" , status: "private")
    assert_not article.save
    assert_equal(["Body is too short (minimum is 10 characters)"], article.errors.full_messages)
  end

  test "article body not empty will pass" do
    article = Article.new(title: "patates", body: "1" , status: "private")
    assert_not article.save
  end

  test "should not save article without status will pass" do
    article = Article.new(title: "patates", body: "patates au four")
    assert_not article.save
  end

  test "should not save article with wrong status will pass" do #right status are: ['public', 'private', 'archived']
    article = Article.new(title: "patates", body: "patates au four", status: "a")
    assert_not article.save
  end

  test "should report error" do
    # some_undefined_variable is not defined elsewhere in the test case
    assert_raises(NameError) do
      some_undefined_variable
    end
  end
end
