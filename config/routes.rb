Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root "articles#index"     #the root route is also mapped to the index action of ArticlesController.
  resources :articles do
    resources :comments
  end

  namespace :api, defaults: { format: 'json' } do
    resources :articles do
      resources :comments, except: [:new, :edit]
    end
  end
end
