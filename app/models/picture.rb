class Picture < ApplicationRecord
    belongs_to :image, polymorphic: true
    has_one_attached :image 
    
end
