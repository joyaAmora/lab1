class Api::CommentsController < ApiController
  before_action :authenticate_user!, only: :destroy

    def index
      article = Article.find(params[:article_id])
      comments = article.comments.all
      render json: comments, status: :created
    end

    def show
      article = Article.find(params[:article_id])
      comments = article.comments.find(params[:id])
      render json: comments, status: :created
    end
  
    def create
        article = Article.find(params[:article_id])
        comment = article.comments.create(comment_params)
        if comment.save
          render json: comment, status: :created
        else
          render json: comment.errors, status: :unprocessable_entity
        end
      end

      def destroy
        article = Article.find(params[:article_id])
        comment = article.comments.find(params[:id])      
        comment.destroy
        if comment.destroy
          render json: comment, status: :created
        else
          render json: comment.errors, status: :unprocessable_entity
        end
      end
    
      private
        def comment_params
          params.require(:comment).permit(:commenter, :body, :status)
        end
end
