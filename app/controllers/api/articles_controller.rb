class Api::ArticlesController < ApiController
  before_action :authenticate_user!, except: [:index, :show]
  def index
    articles = Article.all
    render json: articles
  end

  def show
    article = Article.find(params[:id])
    render json: article
  end

  def new
    article = Article.new
    article.picture = Picture.new
  end

  def create
    article = Article.new(article_params)
    if article.save
      render json: article, status: :created, location: article
    else
      render json: article.errors, status: :unprocessable_entity
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    article = Article.find(params[:id])

    if article.update(article_params)
      render json: article, status: :created, location: article
    else
      render json: article.errors, status: :unprocessable_entity
    end
  end

  def destroy
    article = Article.find(params[:id])
    article.destroy

    redirect_to root_path
  end

  private
    def article_params
      params.require(:article).permit(:title, :body, :status, picture_attributes: [ :id, :name, :image ])
    end
end